// This project was made on the very day of 06/09/16
// By Jonas

#include <iostream>


// Tries to make a class

class Owl{
    private:

        char color[] = "brown";

    public:
        void makeSound(){

            std::cout << "My color is " << color;
        }
};

int main(int argc, char const *argv[]) {

  std::cout << "Hello world" << std::endl;

  int t;
  std::cout << "Skriv inn ett tall: ";
  std::cin >> t;

  std::cout << "Hello world tall: "
            << t
            << std::endl;
  return 0;
}
